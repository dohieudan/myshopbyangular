import { Component } from '@angular/core';
import { LoginService } from '../services/login.service';
@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css'],
})
export class ProductItemComponent {
  results: any[] = [];
  constructor(private _loginService: LoginService) {}
  ngOnInit() {
    this._loginService
      .getData()
      .subscribe((res: any) => (this.results= res.data));
    
  }
}
